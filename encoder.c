/*
	AUTHOR: PRABAL VASHISHT
	START DATE: 28/11/16
	END DATE: 1/12/2016
*/
#include<stdlib.h>
#include<stdio.h>
struct list {
		char alphabet;
		int freq;
		struct list *next;
};
struct tree {
		char alphabet;
		int freq;
		struct tree *rightChild;
		struct tree *leftChild;
};
struct key{
		char key;
		struct key *next;
};
void printKey(struct tree *,FILE *);
void makeKey(char );			// makeKey IS USED FOR MAKING THE LIST OF 0s AND 1s
void push(char);			//push IS USED FOR MAKING THE LIST OF ALPHABETS AND THEIR FREQUENCIES
int inList(char);
void popKey();
struct list pop();
		
struct list *head=NULL;
struct list *repeat;
struct key *headKey;	// USED FOR TRACKING THE START OF THE LINKED LIST
struct key *tailKey;	// USED FOR TRACKING THE END OF THE LINKED LIST

int length=0;		// LENGTH IS USED TO FOR TWO LINKED LISTS (KEY AND LIST)
void main()
{
	int i,j;
	char fname[10];
	char ch;
	FILE *fp, *fpk;				// fpk IS FOR THE FILE IN WHICH THE ENCODING KEY IS TO BE STORED
	printf("Enter the file name:\n");
	scanf("%s",fname);
	fp=fopen(fname,"r");
	if(fp==NULL){
		printf("Sorry the file couldn't be opened");
		return;
	}
	ch=fgetc(fp);
	while(ch!=EOF){
		if(inList(ch)==1)
			repeat->freq+=1;
		else
			push(ch);
		ch=fgetc(fp);
	}
	struct tree *array[length];
	struct list element;
	struct tree *temp;
	for(i=0;i<length;i++){
		if(i==0){
			element=pop();
			array[i]=(struct tree *)malloc(sizeof(struct tree));
			array[i]->alphabet=element.alphabet;
			array[i]->freq=element.freq;
			array[i]->rightChild=NULL;
			array[i]->leftChild=NULL;
		}
		else{
			element=pop();
			array[i]=(struct tree *)malloc(sizeof(struct tree));
			array[i]->alphabet=element.alphabet;
			array[i]->freq=element.freq;
			array[i]->rightChild=NULL;
			array[i]->leftChild=NULL;
			for(j=i;array[j]->freq>=array[j-1]->freq&&j>=1;j--){		//INSERTION SORT
				temp=array[j];
				array[j]=array[j-1];
				array[j-1]=temp;
			}
		}
				
	}
	struct tree *eleTree;
	while(length>1){
		eleTree=(struct tree *)malloc(sizeof(struct tree));
		eleTree->freq=array[length-1]->freq + array[length-2]->freq;
		eleTree->rightChild=array[length-1];
		eleTree->leftChild=array[length-2];
		array[length-2]=eleTree;
		for(j=length-2;array[j]->freq>=array[j-1]->freq&&length>2;j--){
			temp=array[j];
			array[j]=array[j-1];
			array[j-1]=temp;
		}
		length--;
	}
	fclose(fp);
	fp=fopen(fname,"r");
	fpk=fopen("key","w");
	length=0;
	printKey(array[0],fpk);
	fclose(fp);
	fclose(fpk);
	printf("\nEncoded keys are present in file \"key\". Thanks!\n");
}

void push(char ch)
{
	struct list *ptr;
	ptr=(struct list *)malloc(sizeof(struct list));
	length++;
	if(length==1){
		ptr->alphabet=ch;
		ptr->freq=1;
		ptr->next=NULL;
		head=ptr;
	}
	else if(length>1){
		ptr->alphabet=ch;
		ptr->freq=1;
		ptr->next=head;
		head=ptr;
	}
}
int inList(char ch)
{
	struct list *ptr;
	ptr=head;
	while(ptr!=NULL){
		if(ptr->alphabet==ch){
			repeat=ptr;
			return(1);
		}
		ptr=ptr->next;
	}
	return (0);

}
struct list pop()
{
	struct list *temp;
	struct list element;
	element.alphabet=head->alphabet;
	element.freq=head->freq;
	temp=head;
	head=head->next;
	free(temp);
	return(element);
}

void printKey(struct tree *root, FILE *fpk)
{
	struct key *tmp;
	if(root->leftChild!=NULL){
		makeKey('0');
		printKey(root->leftChild,fpk);
	}
	else if(root->leftChild==NULL){
		fputc(root->alphabet,fpk);
		fputc(':',fpk);
		tmp=headKey;
		while(tmp!=NULL){
			fputc(tmp->key,fpk);
			tmp=tmp->next;
		}
		fputc('\n',fpk);
		return;
	}
	popKey();
	if(root->rightChild!=NULL){
		makeKey('1');
		printKey(root->rightChild,fpk);
	}
	else if(root->rightChild==NULL){
		fputc(root->alphabet,fpk);
		fputc(':',fpk);
		tmp=headKey;
		while(tmp!=NULL){
			fputc(tmp->key,fpk);
			tmp=tmp->next;
		}
		fputc('\n',fpk);
		return;
	}
	popKey();
}

void makeKey(char ch)
{
	struct key *ptr;
	ptr=(struct key *)malloc(sizeof(struct key));
	length++;
	if(length==1){
		headKey=ptr;
		tailKey=ptr;
		ptr->key=ch;
		ptr->next=NULL;
	}
	else if(length>1){
		tailKey->next=ptr;
		tailKey=ptr;
		ptr->key=ch;
		ptr->next=NULL;
	}
}	
void popKey()
{
	struct key *ptr1;
	struct key *ptr2;
	ptr1=headKey;
	ptr2=NULL;
	while(ptr1!=tailKey){
		ptr2=ptr1;
		ptr1=ptr1->next;
	}
	free(tailKey);
	length--;
	tailKey=ptr2;
}	

	
	

				
				
		
		
		
	
	
